import os
import json
import copy
import shutil
def main():
    fitbitdataset_directory = "fitbitdataset"
    new_fitbitdataset_directory = "new_fitbitdataset"
    
    dirs = [filename for filename in os.listdir(fitbitdataset_directory) if filename.isdigit()]
    try:
        os.makedirs(new_fitbitdataset_directory)
    except:
        pass
    for directory in dirs:
        try:
            try:
                os.makedirs(os.path.join(new_fitbitdataset_directory,directory))
                os.makedirs(os.path.join(new_fitbitdataset_directory,directory, "labeled_activities"))
                try:
                    shutil.copytree( os.path.join(fitbitdataset_directory,directory, "tagged_locations"), os.path.join(new_fitbitdataset_directory,directory, "tagged_locations"))
                    shutil.copytree( os.path.join(fitbitdataset_directory,directory, "recorded_locations"), os.path.join(new_fitbitdataset_directory,directory, "recorded_locations"))
                except Exception as e:
                    pass
            except Exception as e:
                pass
                
            activity_dir = os.path.join(fitbitdataset_directory,directory, "labeled_activities")
            activity_files = [filename for filename in os.listdir(activity_dir) if filename.startswith("activity")]
            
            for filename in activity_files:
                with open(os.path.join(activity_dir, filename)) as data_file:    
                    activity_json = json.load(data_file)
                    
                    start = activity_json["start"]
                    
                    end = activity_json["end"]
                    
                    #Time format is "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
                    #Don't need to convert to datetime because all strings are the same length and already sequential.
                    locations = sorted(activity_json["locations"], key = lambda x: x["t"])
                    
                    new_json = copy.deepcopy(activity_json)
                    new_json["start"] = locations[0]["t"]
                    new_json["end"] = locations[-1]["t"]
                    new_file_name = "activity_" + new_json["start"].replace(":","_") + "_to_" + new_json["end"].replace(":","_") + ".txt"
                                       
                    with open(os.path.join(new_fitbitdataset_directory,directory,"labeled_activities",new_file_name), 'w') as outfile:
                        json.dump(new_json, outfile)
                        
    
        except Exception as e:
            print(e)
            print(directory, "doesn't have an activity_dir")
        
    print(dirs)
main()
