#!/usr/bin/python3
"""
https://debian-administration.org/article/349/Setting_up_an_SSL_server_with_Apache2

http://man.he.net/man8/a2enmod

https://pypi.python.org/pypi/pycrypto
"""
from flask import Flask
import flask
import json
import os
import datetime
import glob
from apns2.client import APNsClient
from apns2.payload import Payload
import math
import requests
import requests.auth

from datetime import timedelta
import time
import atexit
from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.triggers.cron import CronTrigger

app = Flask(__name__)
#http://www.appicon.build/ for 1024 x 1024 app icon
users = {"mattsap":"a", \
         "ningjing":"awesome", \
         "Butters":"1234", \
         "301":"aaa",\
         "302":"bbb",\
         "303":"ccc",\
         "304":"ddd",\
         "305":"eee",\
         "306":"fff",\
         "307":"ggg",\
         "308":"hhh",\
         "309":"iii",\
         "310":"jjj",\
         "311":"kkk",\
         "401":"aaa",\
         "402":"bbb",\
         "403":"ccc",\
         "404":"ddd",\
         "405":"eee",\
         "406":"fff",\
         "407":"ggg",\
         "408":"hhh",\
         "409":"iii",\
         "410":"jjj",\
         "501":"aaa",\
         "502":"bbb",\
         "503":"ccc",\
         "504":"ddd",\
         "505":"eee",\
         "506":"fff",\
         "507":"ggg",\
         "508":"hhh",\
         "509":"jjj",\
         "510":"kkk"}

# TODO: Double check these with Matt
outdoorsList =  {"home": False, \
           "home--someone else": False, \
           "work": False, \
           "school": False, 
           "gym": False, 
           "park": True, 
           "recreational area": False, \
           "restaurant": False, \
           "other": True}

user_directory = os.path.join("users","%s")
tagged_directory = os.path.join(user_directory,"tagged_locations")
recorded_directory = os.path.join(user_directory, "recorded_locations")
activity_directory = os.path.join(user_directory, "labeled_activities")
message_directory = os.path.join(user_directory, "messages")
response_directory = os.path.join(user_directory, "responses")

tagged_file = os.path.join(tagged_directory, "tagged_%s.txt")
activity_file = os.path.join(activity_directory, "activity_%s_to_%s.txt")
recorded_file = os.path.join(recorded_directory, "recorded_%s.txt")
message_file = os.path.join(message_directory, "message_%s.txt")
response_file = os.path.join(response_directory, "response_%s.txt")

notification_file = "NotificationTokens.json"
fitbit_file = "FitbitTokens.json"

intensity_url = "https://api.fitbit.com/1/user/-/activities/calories/date/%s/1d/15min/time/%s/%s.json" # date (YYYY-MM-DD), start time, end time (hh:mm)... start time and end time should be the same value (current time)
steps_url = "https://api.fitbit.com/1/user/-/activities/steps/date/%s/1d/1min/time/%s/%s.json"

minSteps = 250

@app.route("/")
def hello():
    return "Hello World!"

@app.route('/login', methods=['POST'])
def login():
    print("got message")
    if not flask.request.json:
        flask.abort(400)
    print(flask.request.json)
    #FYI: dicts are not jsons! =)
    username = flask.request.json.get("user","")
    passcode = flask.request.json.get("pass","")
    login_status = "user not found"
    if username in users:
        login_status = "granted" if users[username] == passcode else "incorrect passcode"
    deviceToken()
    return flask.jsonify({'status' : "success", "login":login_status}), 201

@app.route("/taglocation", methods=['POST'])
def tag_location():
    """
    This is called when the user deliberately gives a GPS location via the location tagger screen
    """
    print("tagging location")
    if not flask.request.json:
        flask.abort(400)
    try:
        username = flask.request.json.get("user", "")
        if not os.path.exists(tagged_directory % username):
            print("directory didn't exist, created one")
            os.makedirs(tagged_directory % username)
        f = open(tagged_file % (username, datetime.date.today()), "a")
        f.write(json.dumps(flask.request.json) + "\n")
        f.close()
    except Exception as e:
        return flask.jsonify({'status' : str(e)}), 201
    return flask.jsonify({'status' : "success"}), 201

@app.route("/recordlocation", methods=['POST'])
def record_location():
    print("recording location")
    if not flask.request.json:
        flask.abort(400)
    try:
        username = flask.request.json.get("user", None)
        if not os.path.exists(recorded_directory % username):
            print("directory didn't exist, created one")
            os.makedirs(recorded_directory % username)
        f = open(recorded_file % (username, datetime.date.today()) , "a")
        f.write(json.dumps(flask.request.json) + "\n")
        print(recorded_file % (username, datetime.date.today()))
        f.close()
    except Exception as e:
        print(str(e))
        return flask.jsonify({'status' : str(e)}), 201
    return flask.jsonify({'status' : "success"}), 201

@app.route('/location', methods=['POST']) 
def activity_location():
    print("activity location")
    if not flask.request.json:
        flask.abort(400)
    try:
        username = flask.request.json.get("user", None)
        if not os.path.exists(activity_directory % username):
            print("directory didn't exist, created one" + activity_directory % username)
            os.makedirs(activity_directory % username)
        start = flask.request.json.get("start","")
        end = flask.request.json.get("end","")
        
        f = open(activity_file % (username,start, end) , "a")
        f.write(json.dumps(flask.request.json) + "\n")
        print(activity_file % (username,start, end))
        f.close()
    except Exception as e:
        print(str(e))
        return flask.jsonify({'status' : str(e)}), 201
    return flask.jsonify({'status' : "success"}), 201


# State consists of: Timestamp, Location_Lat, Location_Lon, Outdoors, Intensity, Driving, 
def getCurrentStateForUser(username):
    state = {}

    # Get current timestamp
    # Morning/evening/night
    state["timestamp"] = str(datetime.datetime.now())

    # Get most recent location
    # Assumes that the last line in the most recently modified file is the most recent recorded_location data
    list_of_files = glob.glob(os.path.join(recorded_directory % username,'*')) # * means all if need specific format then *.csv
    if len(list_of_files) > 0:
        latest_file = max(list_of_files, key=os.path.getctime)
        file = open(latest_file)
        lines = list(file)
        file.close()
        data = json.loads(lines[len(lines)-1])
        state["location_lat"] = data["lat"]
        state["location_lon"] = data["long"]
        
        # Driving
        # Speed greater than another value
        state["driving"] = data["speed"] > 13

        # Get Outdoors
        # Default to outdoors. Get radius distance to nearest tagged location and see if its indoors or outdoors if within certain distance.
        # Maximum radius. Get all tagged locations within a certain max radius and then find the closest within that radius.
        # If outside radius then its outside the tagged location.
        taggedLocations = getTaggedLocationsForUser(username)
        closestLocation = min(taggedLocations, key=lambda taggedLocation: distance((data["lat"], data["long"]), (taggedLocation["lat"], taggedLocation["long"])))
        outdoors = "default"
        if distance((data["lat"], data["long"]), (closestLocation["lat"],closestLocation["long"])) < 15:
            outdoors = closestLocation["outdoors"]
        if outdoors == "default":
            outdoors = outdoorsList[closestLocation["location_label"]]
        state["outdoors"] = outdoors
    else:
        print("No location data found for user: " + str(username))

    # Get Intensity
    # https://api.fitbit.com/1/user/-/activities/calories/date/2017-10-13/1d/15min/time/12:43/12:43.json
    heads = {"Authorization":"Bearer " + getFitbitAccessTokenForUser(username)}
    now = datetime.datetime.now()
    response = requests.get(intensity_url % (now.strftime("%Y-%m-%d"), now.strftime("%H:%M"), now.strftime("%H:%M")), headers=heads)

    state["intensity"] = response.json()["activities-calories-intraday"]["dataset"][0]["level"]

    print("Current state for " + username + " is: " + str(state))
    return state


def getTaggedLocationsForUser(user):
    list_of_files = glob.glob(os.path.join(tagged_directory % user,'*')) # * means all if need specific format then *.csv
    tagged_locations = []
    for file in list_of_files:
       with open(file) as f:
            lines = f.readlines()
            lines = [x.strip('\n') for x in lines]
            tagged_locations = [json.loads(x) for x in lines]
    return tagged_locations

# Get distance between two points on earth.
# https://gist.github.com/rochacbruno/2883505
def distance(origin, destination):
    lat1, lon1 = origin
    lat2, lon2 = destination
    # radius = 6371 # km
    radius = 6371 * 1000 # m

    dlat = math.radians(lat2-lat1)
    dlon = math.radians(lon2-lon1)
    a = math.sin(dlat/2) * math.sin(dlat/2) + math.cos(math.radians(lat1)) \
        * math.cos(math.radians(lat2)) * math.sin(dlon/2) * math.sin(dlon/2)
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1-a))
    d = radius * c

    return d

def send_notification(username,message):
    # Check if user token exists
    deviceTokens = json.load(open(notification_file,"r"))
    notificationsToken = deviceTokens.get(username)
    if notificationsToken != None:
        token_hex = notificationsToken
        payload = Payload(alert=message, sound="default", badge=0)
        topic = 'com.seanwhiteman.LocationTracker'
        client = APNsClient('FitbitPushNotificationCertificate.pem', use_sandbox=True, use_alternative_port=False)
        client.send_notification(token_hex, payload, topic)
        return True
    else:
        print("ERROR: There is no token for user: " + username)
        return False

def storeMessage(username,message,location,outdoors,intensity,driving,timestamp):
    data = {'username':username,'message':message,'LocationValue':location,'OutdoorsValue':outdoors,'IntensityValue':intensity,'DrivingValue':driving,'timestamp':str(timestamp)}
    try:
        if not os.path.exists(message_directory % username):
            print("directory didn't exist, created one")
            os.makedirs(message_directory % username)
        f = open(message_file % (username, timestamp), "a")
        f.write(json.dumps(data) + "\n")
        f.close()
    except Exception as e:
        print(str(e))
        return e
    return "Success"

@app.route('/getRecentMessage', methods=['GET'])
def getRecentMessage():
    print(flask.request.url)
    print(list(flask.request.args))
    username = flask.request.args.get('user',None)
    list_of_files = glob.glob(os.path.join(message_directory % username,'*')) # * means all if need specific format then *.csv
    print(message_directory % username)
    print(list_of_files)
    if len(list_of_files) > 0:
        latest_file = max(list_of_files, key=os.path.getctime)
        print(latest_file)
        data = json.load(open(latest_file))

        return flask.jsonify({'status':'success','message':data['message'],'LocationValue':data['LocationValue'],'OutdoorsValue':data['OutdoorsValue'],'IntensityValue':data['IntensityValue'],'DrivingValue':data['DrivingValue'],'timestamp':data['timestamp']})
    else:
        return flask.jsonify({'status':'success','message':'No message found.'})
        # Should be a success. Message saying no file to be found or something like that.

@app.route('/respondToMessage', methods=['POST'])
def respondToMessage():
    username = flask.request.json.get("user",None)
    timestamp = flask.request.json["timestamp"]
    try:
        if not os.path.exists(response_directory % username):
            print("directory didn't exist, created one")
            os.makedirs(response_directory % username)
        f = open(response_file % (username, timestamp), "a")
        f.write(json.dumps(flask.request.json) + "\n")
        f.close()
    except Exception as e:
        print(str(e))
        return e
    return "Success"

@app.route('/deviceToken', methods=['POST'])
def deviceToken():
    username = flask.request.json.get("user",None)
    notificationsToken = flask.request.json["notificationsToken"]
    try:
        f = open(notification_file,"r+")
        fileContents = json.load(f)
        fileContents[username] = notificationsToken
        f.seek(0)
        f.truncate()
        f.write(json.dumps(fileContents))
        f.close()
    except Exception as e:
        print(str(e))
        return e
    return "Success"

@app.route('/sendTestNotif301',methods=['GET'])
def sendTestNotif301():
    send_notification("305","Hey you should go work out!")
    return "Success"

@app.route('/sendTestNotifMattsap',methods=['GET'])
def sendTestNotifMattsap():
    send_notification("301","Hey you should go work out!")
    return "Success"





# OAUTH 2
# Not a full implementation. 
# Must already have a stored access token and refresh token.
# getFitbitAccessTokenForUser should get an active access token (by refreshing the token if necessary).

CLIENT_ID = "22CK47"
CLIENT_SECRET = "8e511e967df43497edd0c9a9c5e7be9b"
REDIRECT_URI = "http://localhost:65010/fitbit_callback"
REFRESH_URL = "https://api.fitbit.com/oauth2/token"
INTROSPECT_URL = "https://api.fitbit.com/oauth2/introspect"

def getFitbitStoredTokensForUser(user):
    print("Loading stored tokens for " + user)
    fitbitTokens = json.load(open(fitbit_file,"r"))
    storedTokens = fitbitTokens.get(user)
    if storedTokens != None:
        return storedTokens
    else:
        return -1
        print("ERROR: There are no stored fitbit tokens for user: " + str(user))

def getFitbitAccessTokenForUser(user):
    print("Finding token for " + user)
    # Check if token is still valid
    storedTokens = getFitbitStoredTokensForUser(user)
    heads = {"Authorization":"Bearer " + storedTokens["access_token"]}
    data = {"token":storedTokens["access_token"]}
    response = requests.post(INTROSPECT_URL, data=data, headers=heads)
    print(response)
    if response.json().get("active",-1) != 1:
        print("Token was not active. Refreshing.")
        return refreshFitbitTokensForUser(user)["access_token"]
    print("Token active.")
    return storedTokens["access_token"]

def refreshFitbitTokensForUser(user):
    print("Refreshing token for " + user)
    basicAuth = requests.auth.HTTPBasicAuth(CLIENT_ID, CLIENT_SECRET)
    storedTokens = getFitbitStoredTokensForUser(user)
    data = {"grant_type":"refresh_token", "refresh_token":storedTokens["refresh_token"]}
    response = requests.post(REFRESH_URL, data=data, auth=basicAuth)
    token_json = response.json()
    try:
        f = open(fitbit_file,"r+")
        fileContents = json.load(f)
        fileContents[user] = token_json
        f.seek(0)
        f.truncate()
        f.write(json.dumps(fileContents))
        f.close()
    except Exception as e:
        print(str(e))
    return token_json

@app.route("/testGetState")
def testGetState():
    return json.dumps(getCurrentStateForUser("301"))

# General Policy

def getStepsForHourForUser(username):
    # Get Intensity
    # https://api.fitbit.com/1/user/-/activities/calories/date/2017-10-13/1d/15min/time/12:43/12:43.json
    heads = {"Authorization":"Bearer " + getFitbitAccessTokenForUser(username)}
    now = datetime.datetime.now()
    response = requests.get(steps_url % (now.strftime("%Y-%m-%d"), (now - timedelta(hours=1)).strftime("%H:%M"), now.strftime("%H:%M")), headers=heads)
    print(response.json())
    steps = 0
    for reading in response.json()["activities-steps-intraday"]["dataset"]:
        steps += reading["value"]
    # steps = response.json()["activities-steps-intraday"]["dataset"][0]["value"]
    return steps

def generalPolicy():
    for user in users:
        if not(getFitbitStoredTokensForUser(user) == -1):
            steps = getStepsForHourForUser(user)
            if steps < minSteps:
                print("sent notification to " + user)
                send_notification(user,"Hey! Go exercise!")

# Main

if __name__ == "__main__":
    #Decker Note: 0.0.0.0 means use the global IP rather than local IP
    #app.run(host='0.0.0.0', port=5011, threaded=True, debug=True)
    # storeMessage(301,"test stored message","home",True,"Vigorous",False,datetime.date.today())
    # print(getStepsForHourForUser("301"))
    generalPolicy()
    # General Policy scheduler
    # Times of day to run?
    scheduler = BackgroundScheduler()
    scheduler.start()
    scheduler.add_job(
        func=generalPolicy,
        trigger=CronTrigger(minute=55),
        id='printing_job',
        name='Print date and time every five seconds',
        replace_existing=True)
    # Shut down the scheduler when exiting the app
    atexit.register(lambda: scheduler.shutdown())

    app.run(host='0.0.0.0', port=5010, threaded=True, debug=False)
    setup_user_directories()


