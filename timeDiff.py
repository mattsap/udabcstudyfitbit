# http://scatter-otl.rhcloud.com/location?lat=39.683000&long=-75.753667

import os
import json
import urllib.request
from datetime import datetime
import dateutil.parser

cities = []
staticPath = "static"
datasetPath = os.path.join(staticPath, "fitbitdataset")
activitiesPath = "labeled_activities"
citiesDatasetPath = "citiesDataset"

outputLines = []

def findCities():
	participants = os.listdir(datasetPath)
	outputFile = open("timeDiff.csv","w")
	outputFile.write("participant,activity file,startDiff,endDiff\n")
	try:
		participants.remove(".DS_Store")
	except Exception as e:
		pass

	for participant in participants:
		print(participant)
		activities = []
		try:
			activities = os.listdir(os.path.join(datasetPath,participant,activitiesPath))
		except Exception as e:
			pass

		for activity in activities:
			activityFile = open(os.path.join(datasetPath,participant,activitiesPath,activity))
			data = json.load(activityFile)
			# print(participant + " " + activity)
			sortedData = sorted(data["locations"], key=lambda x: dateutil.parser.parse(x["t"]))
			start = dateutil.parser.parse(data["start"])
			end = dateutil.parser.parse(data["end"])
			beforeBeginTime = False
			afterEndTime = False
			startDifference = -1
			endDifference = -1
			if(len(sortedData) > 0):
				smallestDPDT = dateutil.parser.parse(sortedData[0]["t"])
				# print(smallestDPDT)
				largestDPDT = dateutil.parser.parse(sortedData[len(sortedData)-1]["t"])
				# print(largestDPDT)

				if(smallestDPDT < start):
					beforeBeginTime = True
					startDifference = start - smallestDPDT
				if(largestDPDT > end):
					afterEndTime = True
					endDifference = largestDPDT - end

			newLine = ",".join([participant, activity, str(startDifference), str(endDifference)])
			newLine += "\n"
			outputLines.append(newLine)
			outputFile.write(newLine)
			print(newLine)
			for datapoint in data["locations"]:
				dt = dateutil.parser.parse(datapoint["t"])
				# newLine = ",".join([participant, activity, dt.strftime()])
				# newLine += "\n"
				# outputLines.append(newLine)
				# outputFile.write(newLine)
				# print(newLine)
			# print(data["locations"][0])
	print("ALL LINES")
	print(outputLines)
	# for line in outputLines:
	# 	outputFile.write(line)
	outputFile.close()

findCities()
