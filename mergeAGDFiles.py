headers = ["Date", " Time", "Axis1","Axis2","Axis3","Steps","HR","Lux","Inclinometer Off","Inclinometer Standing","Inclinometer Sitting","Inclinometer Lying",
"Vector Magnitude"]
import csv
from datetime import datetime
from datetime import timedelta
from itertools import islice


        
def main(): 
    filename1 = "106 (2017-05-10)60secDataTable.csv"
    filename2 = "106 (2017-05-15)60secDataTable.csv"
    filename_export = "106 (2017-15-10)60secDataTable_merged.csv"
    
    data_from_file1 = []
    #open file and
    with open(filename1) as f:
        csv_reader = csv.reader(islice(f, 10,None))
        headers = next(csv_reader)
        for row in csv_reader:
            data_from_file1.append(row)

    data_from_file2 = []
    with open(filename2) as f:
        csv_reader = csv.reader(islice(f, 10,None))
        headers = next(csv_reader)
        
        for row in csv_reader:
            data_from_file2.append(row)

    #extract the information before the headers in the actigraph file
    f = open(filename1,"r")
    file1_preamble = "".join(f.readlines()[:11])
    f.close()

    inserted_data = []
    

    fmt = '%m/%d/%Y %H:%M:%S'
    file1_end = datetime.strptime(" ".join(data_from_file1[-1][:2]), fmt)
    file2_start = datetime.strptime(" ".join(data_from_file2[0][:2]), fmt)

    current = file1_end + timedelta(minutes = 1)
    while current != file2_start:
        inserted_data.append(current)
        current = current + timedelta(minutes = 1)
        
        

    f = open(filename_export, "w")
    f.write(file1_preamble)
    f.write("\n".join(list(map(lambda x: ",".join(x),data_from_file1))))
    f.write("\n")
    f.write("\n".join(list(map(lambda x: ",".join(str(x).split(" ")) + "," + ",".join(["0" for i in headers[2:]]), inserted_data))))
    f.write("\n")
    f.write("\n".join(list(map(lambda x: ",".join(x),data_from_file2))))
    f.close()
    

    

    

    

main()
