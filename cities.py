# http://scatter-otl.rhcloud.com/location?lat=39.683000&long=-75.753667

import os
import json
import urllib.request
from datetime import datetime
import dateutil.parser

cities = []
staticPath = "static"
datasetPath = os.path.join(staticPath, "fitbitdataset")
activitiesPath = "labeled_activities"
citiesDatasetPath = "citiesDataset"

outputLines = []

def findCities():
	participants = os.listdir(datasetPath)
	outputFile = open("cities.csv","w")
	try:
		participants.remove(".DS_Store")
	except Exception as e:
		pass

	for participant in participants:
		print(participant)
		activities = []
		try:
			activities = os.listdir(os.path.join(datasetPath,participant,activitiesPath))
		except Exception as e:
			pass

		for activity in activities:
			activityFile = open(os.path.join(datasetPath,participant,activitiesPath,activity))
			data = json.load(activityFile)
			for datapoint in data["locations"]:
				url = "http://scatter-otl.rhcloud.com/location?lat=" + str(datapoint["lat"]) + "&long=" + str(datapoint["lon"])
				webData = json.loads(urllib.request.urlopen(url).read().decode())
				dt = dateutil.parser.parse(datapoint["t"])
				print(webData["city"])
				# newLine = dt.strftime('%m/%d/%Y') + "," + dt.strftime('%H/%M/%S') + ","
				newLine = ",".join([dt.strftime('%x'),dt.strftime('%X'),webData["city"],str(datapoint["lat"]),str(datapoint["lon"]),data["user"]])
				newLine += "\n"
				outputLines.append(newLine)
				outputFile.write(newLine)
				print(newLine)
			# print(data["locations"][0])
	print("ALL LINES")
	print(outputLines)
	# for line in outputLines:
	# 	outputFile.write(line)
	outputFile.close()

findCities()
