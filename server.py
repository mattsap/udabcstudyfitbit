#!/usr/bin/python3
"""
https://debian-administration.org/article/349/Setting_up_an_SSL_server_with_Apache2

http://man.he.net/man8/a2enmod

https://pypi.python.org/pypi/pycrypto
"""
from flask import Flask
import flask
from flask import request
import json
import os
import datetime
app = Flask(__name__,static_url_path="/static")

datasetPath = "fitbitdataset"
newdatasetPath = "newdataset"
staticPath = "static"

# Hello World
@app.route("/")
def hello():
    return "Hello World! Please visit /visualizer"

# Upload route. Removes any "COM-" prefix and saves the file to the New Dataset. Old dataset files are not touched.
@app.route('/upload', methods=['POST'])
def upload():
    print("Upload")
    if not flask.request.json:
        flask.abort(400)
    print(flask.request.json)

    filename = remove_prefix(str(request.json["filename"]),"COM-")


    if not os.path.exists(os.path.join(staticPath, newdatasetPath, str(request.json["id"]), str(request.json["type"]))):
        os.makedirs(os.path.join(staticPath, newdatasetPath, str(request.json["id"]), str(request.json["type"])))

    file = open(os.path.join(staticPath, newdatasetPath, str(request.json["id"]), str(request.json["type"]), filename),"w")
    file.write('"'.join(str(request.json["file"]).split("'")))
    file.close()
    
    return "Success"

# Gets a list of all the participants
@app.route('/getParticipants', methods=['GET'])
def getParticipants():
    # returns list of participants within a json object
    participants = os.listdir(os.path.join(staticPath, datasetPath))
    try:
        participants.remove(".DS_Store")
    except Exception as e:
        pass
    return flask.jsonify({'participants':participants}),201

# Gets a list of all the participant's files for a given participant id and type(tagged locations/labeled activities/recorded locations)
@app.route('/getParticipantFiles', methods=['GET'])
def getParticipantFiles():
    print("getParticipantFiles")
    # gets list of files available for participant and type (labeled_activity, etc.)
    files = os.listdir(os.path.join(staticPath, datasetPath, request.args["id"], request.args["type"]))
    for i in range(len(files)):
        if(os.path.isfile(os.path.join(staticPath, newdatasetPath, request.args["id"], request.args["type"], files[i]))):
            files[i] = "COM-" + files[i]
            print(files[i])
    print(files)
    return flask.jsonify({'files':files}),201

# Gets a file for participant id, type, and file name. If it starts with "COM-" then the file is loaded from the new dataset.
@app.route('/getParticipantFile', methods=['GET'])
def getParticipantFile():
    print("getParticipantFile")
    # return "False"
    # gets file for participant name
    if(str.startswith(str(request.args["file"]),"COM-")):
        print("New Dataset")
        return app.send_static_file(os.path.join(newdatasetPath, str(request.args["id"]), str(request.args["type"]), remove_prefix(str(request.args["file"]),"COM-")))
    else:
        print("Old Dataset")
        return app.send_static_file(os.path.join(datasetPath, str(request.args["id"]), str(request.args["type"]), str(request.args["file"])))

# sends the visualizer html file
@app.route('/visualizer', methods=['GET']) 
def visualizer():
    print("Visualizer");
    return app.send_static_file("visualizer.html");

def remove_prefix(text, prefix):
    if text.startswith(prefix):
        return text[len(prefix):]
    return text  

@app.after_request
def add_header(response):
    """
    Add headers to both force latest IE rendering engine or Chrome Frame,
    and also to cache the rendered page for 10 minutes.
    """
    response.headers['X-UA-Compatible'] = 'IE=Edge,chrome=1'
    response.headers['Cache-Control'] = 'public, max-age=0'
    return response


if __name__ == "__main__":
    #Decker Note: 0.0.0.0 means use the global IP rather than local IP
    app.run(host='0.0.0.0', port=5010, threaded=True, debug=True)
    

