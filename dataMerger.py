# http://scatter-otl.rhcloud.com/location?lat=39.683000&long=-75.753667

import os
import json
import urllib.request
from datetime import datetime,timedelta
import dateutil.parser
import csv
from itertools import islice

cities = []
staticPath = "static"
datasetPath = os.path.join(staticPath, "newdataset_labeled")
activitiesPath = "labeled_activities"
citiesDatasetPath = "citiesDataset"
mergedDatasetPath = "Merged"
recordedLocationsPath = "recorded_locations"
actigraphPath = "actigraph"

outputLines = []

headers = ["Date", " Time", " Axis1","Axis2","Axis3","Steps","HR","Lux","Inclinometer Off","Inclinometer Standing","Inclinometer Sitting","Inclinometer Lying",
"Vector Magnitude", "city","researcher_tag","details","activity","researcher_rating","speed"]


def mergeActigraphFileWithActivities(actigraph,participant):
	data = getDataFromActigraphFile(actigraph)
	writeToFile(participant,data)
	print("Merge actigraph with activities: " + actigraph + " " + str(participant))
	activities = []
	# count = 0
	first = True
	try:
		activities = os.listdir(os.path.join(datasetPath,str(participant),activitiesPath))
		activities.remove(".DS_Store")
	except Exception as e:
		print(e)
	for activity in activities:
		activityFilePath = os.path.join(datasetPath,str(participant),activitiesPath,activity)
		mergeDataFromFiles(actigraph,activityFilePath,participant,first)
		first = False
	# print(actigraph)


# TODO: Does not account for new file with modified headers and heading. Make new one for other version of data
def getDataFromActigraphFile(fileName):
	print("From actigraph: " + fileName)
	data = []
	#open file and
	with open(fileName) as f:
		csv_reader = csv.reader(islice(f, 10,None))
		headers = next(csv_reader)
		
		for row in csv_reader:
			#convert row to json with headers as keys and row as values
			row_as_json = {headers[i]:row[i] for i in range(len(headers))}
			data.append(row_as_json)
	return data

def getDataFromInProgressMergingFile(fileName):
	print("In progress merging: " + fileName)
	data = []
	#open file and
	try:
		with open(fileName) as f:
			csv_reader = csv.reader(f)
			headers = next(csv_reader)
			
			for row in csv_reader:
				# print(str(fileName.split(" ")[0]) + " " + str(row))
				#convert row to json with headers as keys and row as values
				row_as_json = {headers[i]:row[i] for i in range(len(row))}
				data.append(row_as_json)
	except FileNotFoundError as e:
		print(e)
	return data

def mergeDataFromFiles(actigraphFileName, activityFileName, participant, first):
	actigraphData = None
	if first:
		actigraphData = getDataFromActigraphFile(actigraphFileName)
	else:
		actigraphData = getDataFromInProgressMergingFile(os.path.join(mergedDatasetPath,str(participant)) + ".csv")
	print(activityFileName)
	activityFile = open(activityFileName,"r",encoding="UTF-8") #open(os.path.join(datasetPath,str(participant),activitiesPath,activityFileName))
	activityData = json.load(activityFile)
	activityFile.close()

	# "2017-05-05T16:54:55.571-0400"
	start = datetime.strptime(activityData["start"],"%Y-%m-%dT%H:%M:%S.%f%z") # dateutil.parser.parse(activityData["start"])
	end = datetime.strptime(activityData["end"],"%Y-%m-%dT%H:%M:%S.%f%z") #dateutil.parser.parse(activityData["end"])

	startIndex, endIndex = findStartAndEndIndexForTimesAndActigraphData(start,end,actigraphData)

	print(str(startIndex) + " " + str(endIndex))

	sortedLocations = sorted(activityData["locations"], key=lambda x: dateutil.parser.parse(x["t"]))

	lastActivityIndex = 0
	for i in range(startIndex,endIndex):
		actigraphDT = dateutil.parser.parse(actigraphData[i]["Date"] + " " + actigraphData[i][" Time"] + "-0400")
		lastTimeDiff = timedelta.max
		nearestNeighbor = None

		#could sort based on absolute time differences and the nearest neighbor is the first element in the list.
		for activityDatapoint in sortedLocations:
			activityDT = dateutil.parser.parse(activityDatapoint["t"])
			timeDiff = abs(activityDT - actigraphDT)
			if(timeDiff < lastTimeDiff):
				nearestNeighbor = activityDatapoint
			lastTimeDiff = timeDiff
		# print(getCityNameForActivityDatapoint(nearestNeighbor))
		actigraphData[i]["city"] = getCityNameForActivityDatapoint(nearestNeighbor)
		actigraphData[i].update(nearestNeighbor)
		actigraphData[i]["details"] = activityData["details"]
		actigraphData[i]["researcher_rating"] = activityData["researcher_rating"]
		actigraphData[i]["activity"] = activityData["activity"]
		actigraphData[i]["speed"] = ""
	# print(actigraphData)
	writeToFile(participant,actigraphData)
	
	return actigraphData

	


		# lastTimeDiff = timedelta.max
		# actualActivityIndex = lastActivityIndex
		# for x in range(lastActivityIndex,len(sortedLocations)):
		# 	datapoint = sortedLocations[actualActivityIndex]
		# 	curTimeDiff = dateutil.parser.parse(datapoint["t"])
			

	# for datapoint in activityData["locations"]:
	# 	dt = dateutil.parser.parse(datapoint["t"])
	# 	print(webData["city"])
	# 	newLine = ",".join([dt.strftime('%x'),dt.strftime('%X'),webData["city"],str(datapoint["lat"]),str(datapoint["lon"]),activityData["user"]])
	# 	newLine += "\n"
	# 	# outputLines.append(newLine)
	# 	# outputFile.write(newLine)
	# 	print(newLine)

def getIndexForTimeInActigraphFile(time,data):
	# print(data)
	firstDT = dateutil.parser.parse(data[0]["Date"] + " " + data[0][" Time"] + "-0400")
	# print("Time: " + str(time) + " FirstDT: " + str(firstDT))
	# print("Derived: " + str(data[((time.replace(second=0, microsecond=0) - firstDT).seconds//60)]["Date"]) + " " + str(data[((time.replace(second=0, microsecond=0) - firstDT).seconds//60)][" Time"]))
	secondsToMinutes = (time.replace(second=0, microsecond=0) - firstDT).seconds//60;
	daysToMinutes = int((time.replace(second=0, microsecond=0) - firstDT).days*24*60);
	derived = secondsToMinutes + daysToMinutes
	return derived;

def findStartAndEndIndexForTimesAndActigraphData(startTime,endTime,data):

	return getIndexForTimeInActigraphFile(startTime,data), getIndexForTimeInActigraphFile(endTime,data)
	# curIndex = 0
	# startIndex = -1
	# endIndex = -1
	# # print(data[curIndex])
	# curDT = dateutil.parser.parse(data[curIndex]["Date"] + " " + data[curIndex][" Time"] + "-0400")
	# while(curDT < endTime):
	# 	curDT = dateutil.parser.parse(data[curIndex]["Date"] + " " + data[curIndex][" Time"] + "-0400")
	# 	if(curDT > startTime and startIndex == -1):
	# 		startIndex = curIndex
	# 	# print(str(curDT))
	# 	# print(data[curIndex])
	# 	curIndex += 1
	# endIndex = curIndex

	# print("")
	# print("NEW: ")

	# print("start " + str(startIndex) + " " + str(getIndexForTimeInActigraphFile(startTime,data)))
	# print("Searched: " + str(data[startIndex]["Date"]) + " " + str(data[startIndex][" Time"]))

	# print("end " + str(endIndex) + " " + str(getIndexForTimeInActigraphFile(endTime,data)))
	# print("Searched: " + str(data[endIndex]["Date"]) + " " + str(data[endIndex][" Time"]))
	# print("")
	# return startIndex,endIndex

# Do this by time stamp not anything else
cityRecords = []
def getCityNameForActivityDatapoint(datapoint):
	if len(cityRecords) == 0:
		with open("cities.csv","r") as f:
			csv_reader = csv.reader(f)
			for row in csv_reader:
				cityRecords.append(row)
				# print(row[0] + row[1] + "       " + dateutil.parser.parse(datapoint["t"]).strftime("%x%X"))
				if row[0] + row[1] == dateutil.parser.parse(datapoint["t"]).strftime("%x%X"):
					return row[2]
				# print(datapoint)
				# print(row[3] + " " + str(datapoint["lat"]))
				# if row[3] == str(datapoint["lat"]) and row[4] == str(datapoint["lng"]):
				# 	return row[2]
	for row in cityRecords:
		if row[0] + row[1] == dateutil.parser.parse(datapoint["t"]).strftime("%x%X"):
			return row[2]
	return ""

rlCityRecords = []
def getCityNameForRLDatapoint(datapoint):
	if len(rlCityRecords) == 0:
		with open("citiesRecordedLocations.csv","r") as f:
			csv_reader = csv.reader(f)
			for row in csv_reader:
				rlCityRecords.append(row)
				# print(row[0] + row[1] + "       " + dateutil.parser.parse(datapoint["t"]).strftime("%x%X"))
				if row[0] + row[1] == dateutil.parser.parse(datapoint["t"]).strftime("%x%X"):
					return row[2]
				# print(datapoint)
				# print(row[3] + " " + str(datapoint["lat"]))
				# if row[3] == str(datapoint["lat"]) and row[4] == str(datapoint["lng"]):
				# 	return row[2]
	for row in rlCityRecords:
		if row[0] + row[1] == dateutil.parser.parse(datapoint["t"]).strftime("%x%X"):
			return row[2]
	return ""
	# with open("citiesRecordedLocations.csv","r") as f:
	# 	csv_reader = csv.reader(f)
	# 	for row in csv_reader:
	# 		# print(row[0] + row[1] + "       " + dateutil.parser.parse(datapoint["t"]).strftime("%x%X"))
	# 		if row[0] + row[1] == dateutil.parser.parse(datapoint["t"]).strftime("%x%X"):
	# 			return row[2]
	# 		# print(datapoint)
	# 		# print(row[3] + " " + str(datapoint["lat"]))
	# 		# if row[3] == str(datapoint["lat"]) and row[4] == str(datapoint["lng"]):
	# 		# 	return row[2]
	# return ""

def mergeActigraphFileWithRecordedLocations(actigraph,participant):
	rlfs = []
	first = True
	rlfd = []
	try:
		rlfs = os.listdir(os.path.join(datasetPath,str(participant),recordedLocationsPath))
		rlfs.remove(".DS_Store")
	except Exception as e:
		print(e)
	for rlf in rlfs:
		with open(os.path.join(datasetPath,str(participant),recordedLocationsPath,rlf)) as f:
			data = json.load(f)
			rlfd += data["readings"]
	mergeDataFromRLFileData(actigraph,rlfd,participant)
		# first = False



def mergeDataFromRLFileData(actigraphFileName, rlfd, participant):
	actigraphData = getDataFromInProgressMergingFile(os.path.join(mergedDatasetPath,str(participant)) + ".csv")

	sortedLocations = sorted(rlfd, key=lambda x: dateutil.parser.parse(x["t"]))

	lastRLIndex = 0
	for i in range(0,len(sortedLocations)):
		if participant == 204:
			print(actigraphData[0])
		index = getIndexForTimeInActigraphFile(dateutil.parser.parse(sortedLocations[i]["t"]),actigraphData)
		# print(str(index) + " " + str(dateutil.parser.parse(sortedLocations[i]["t"])) + " " + str(len(actigraphData)) + " " + str(dateutil.parser.parse(actigraphData[len(actigraphData)-1]["Date"] + " " + actigraphData[len(actigraphData)-1][" Time"] + "-0400")))
		if index >= len(actigraphData) or actigraphData[index]["researcher_tag"] != "None":
			continue
		actigraphData[index]["city"] = getCityNameForRLDatapoint(sortedLocations[i])
		actigraphData[index].update(sortedLocations[i])
		actigraphData[index]["details"] = ""
		actigraphData[index]["researcher_rating"] = -1
		actigraphData[index]["activity"] = ""
		if "speed" in sortedLocations[i]:
			actigraphData[index]["speed"] = sortedLocations[i]["speed"]
		else:
			actigraphData[index]["speed"] = ""

	# for i in range(0,len(actigraphData)):
	# 	if actigraphData[i]["researcher_tag"] != "None":
	# 		continue
	# 	actigraphDT = dateutil.parser.parse(actigraphData[i]["Date"] + " " + actigraphData[i][" Time"] + "-0400")
	# 	lastTimeDiff = timedelta.max
	# 	nearestNeighbor = None
	# 	for rlDatapoint in sortedLocations:
	# 		rlDT = dateutil.parser.parse(rlDatapoint["t"])
	# 		timeDiff = abs(rlDT - actigraphDT)
	# 		if(timeDiff < lastTimeDiff):
	# 			nearestNeighbor = rlDatapoint
	# 		lastTimeDiff = timeDiff
	# 	# print(nearestNeighbor)
	# 	# print(getCityNameForActivityDatapoint(nearestNeighbor))
	# 	actigraphData[i]["city"] = getCityNameForRLDatapoint(nearestNeighbor)
	# 	actigraphData[i].update(nearestNeighbor)
	# 	actigraphData[i]["details"] = ""
	# 	actigraphData[i]["researcher_rating"] = -1
	# 	actigraphData[i]["activity"] = ""
	# 	if "speed" in nearestNeighbor:
	# 		actigraphData[i]["speed"] = nearestNeighbor["speed"]
	# 	else:
	# 		actigraphData[i]["speed"] = ""
	# print(actigraphData)
	writeToFile(participant,actigraphData)


def writeToFile(participant,data):
	outFile = open(os.path.join(mergedDatasetPath,str(participant) + ".csv"), "w")
	outFile.write(",".join(headers) + ",\n")
	# Date, Time, Axis1,Axis2,Axis3,Steps,HR,Lux,Inclinometer Off,Inclinometer Standing,Inclinometer Sitting,Inclinometer Lying,Vector Magnitude
	for row in data:
		# print(row)
		line = ",".join([str(row.get(header,"None")).replace(",","/") for header in headers])
		outFile.write(line + ",\n")
	outFile.close()

# mergeDataFromFiles("actigraph/101 (2017-05-12)60secDataTable.csv","activity_2017-05-05T16:28:39.123-0400_to_2017-05-05T16:56:05.266-0400.txt")
# mergeActigraphFileWithActivities("actigraph/101 (2017-05-12)60secDataTable.csv",101)
# mergeActigraphFileWithRecordedLocations("actigraph/101 (2017-05-12)60secDataTable.csv",101)


def main():
	actigraphFiles = []
	try:
		actigraphFiles = os.listdir(actigraphPath)
		actigraphFiles.remove(".DS_Store")
	except Exception as e:
		print(e)

	for actigraphFile in actigraphFiles:
		path = os.path.join(actigraphPath, actigraphFile)
		participant = int(actigraphFile.split(" ")[0])
		mergeActigraphFileWithActivities(path,participant)
		mergeActigraphFileWithRecordedLocations(path,participant)


main()




