CLIENT_ID = "22CK47"
CLIENT_SECRET = "8e511e967df43497edd0c9a9c5e7be9b"
REDIRECT_URI = "http://128.4.101.123:65010/fitbit_callback"
import logging

import json
from flask import Flask
from uuid import uuid4
import urllib.parse
from flask import abort, request
import requests
import requests.auth
import sys
app = Flask(__name__)

###MATT ADDED THIS
def user_agent():
    '''reddit API clients should each have their own, unique user-agent
    Ideally, with contact info included.

    e.g.,
    return "oauth2-sample-app by /u/%s" % your_reddit_username
    '''
    return "flask-server-example"

###MATT ADDED THIS
def base_headers():
    return {"User-Agent": user_agent()}

@app.route('/')
def homepage():
    text = '<a href="%s">Authenticate with fitbit</a>'
    return text % make_authorization_url()

def make_authorization_url():
    # Generate a random string for the state parameter
    # Save it for use later to prevent xsrf attacks

    state = str(uuid4())
    save_created_state(state)
    params = {"client_id": CLIENT_ID,
              "response_type": "code",
              "redirect_uri": REDIRECT_URI,
              "scope": "activity heartrate location"}
    ###MATT ADDED THIS. urllib.parse.urlencode(params)
    url = "https://www.fitbit.com/oauth2/authorize?" + urllib.parse.urlencode(params)
    return url

# Left as an exercise to the reader.
# You may want to store valid states in a database or memcache,
# or perhaps cryptographically sign them and verify upon retrieval.
def save_created_state(state):
    pass
def is_valid_state(state):
    return True

@app.route('/fitbit_callback')
def fitbit_callback():
    print("1", request.args)
    error = request.args.get('error', '')
    if error:
        return "Error: " + error
    state = request.args.get('state', '')
    if not is_valid_state(state):
        # Uh-oh, this request wasn't started by us!
        print("aborted")
        abort(403)
    code = request.args.get('code')
    # We'll change this next line in just a moment
    access_token = get_token(code)
    return json.dumps(get_user_xid(access_token))

def get_token(code):
    ###MATT ADDED THIS
    heads = base_headers()


    client_auth = requests.auth.HTTPBasicAuth(CLIENT_ID, CLIENT_SECRET)
    post_data = {"grant_type": "authorization_code",
		 "client_id" : CLIENT_ID,
		 "client_secret" : CLIENT_SECRET,
                 "code": code,
                 "redirect_uri": REDIRECT_URI}

    ###MATT ADDED headers=heads
    response = requests.post("https://api.fitbit.com/oauth2/token",
		             auth=client_auth,
			     data=post_data,
			     headers=heads
			     )
    token_json = response.json()
    print('TOKEN:::: ', token_json)
    return token_json["access_token"]

def get_user_xid(access_token):
    ###MATT ADDED THIS
    headers = base_headers()
    headers.update({"Authorization": "Bearer " + access_token})
# https://jawbone.com/nudge/api/v.1.1/users/@me/moves?start_time=1457586000&limit=1000
    # response = requests.get("https://api.fitbit.com/1/user/-/activities/calories/date/2017-10-13/1d/1min/time/12:30/12:45.json", headers=headers)
    # response = requests.get("https://api.fitbit.com/1/user/-/activities/steps/date/2017-10-13/1d/1min/time/12:30/12:45.json", headers=headers)
    # response = requests.get("https://api.fitbit.com/1/user/-/activities/heart/date/2017-10-13/1d/1min.json", headers=headers)
    # response = requests.get("https://api.fitbit.com/1/user/-/activities/heart/date/2017-10-17/1d/1min/time/00:00/23:59.json", headers=headers)
    response = requests.get("https://api.fitbit.com/1/user/-/activities/steps/date/2017-10-13/1d/15min/time/10:30/12:45.json", headers=headers)

    me_json = response.json()
    print(me_json)
    return me_json

if __name__ == '__main__':
    print("hi")
    app.run(host="0.0.0.0",debug=True, port=65010)

    obj = open('data.json','wb')

    with app.test_request_context():
        obj.write((fitbit_callback()))





